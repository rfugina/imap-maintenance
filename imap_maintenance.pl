#!/usr/bin/perl

use Data::Dumper;
use Email::Simple;
use File::Slurp;
use Getopt::Long;
use Net::IMAP::Simple;
use Net::IMAP::Simple::Gmail;

use strict;

$|=1;

my $config_file;
my $reverse = 0; # reverse order of message processing
my $repeat = 0; # repeat until no changes
my $verbose = 0;
my $debug = 0;
my $limit = 2000000000000; # max operations to process
my $interval = 2000000000000; # commit interval
my $notreally = 0;
my $showme = -1;

GetOptions(
    "config=s"  => \$config_file,
    "debug"     => \$debug,
    "notreally" => \$notreally,
    "repeat"    => \$repeat,
    "reverse"   => \$reverse,
    "interval"  => \$interval,
    "limit"     => \$limit,
    "showme=i"  => \$showme,
    "verbose"   => \$verbose,
) or die "Invalid cmdline options";

$verbose = 1 if $debug;

die "No config or doesn't exist" unless $config_file && -r $config_file;

my $config = eval(read_file($config_file));
die $@ if $@;

my $type = $config->{type} || die "No type in config";
my $server = $config->{server} || die "No server in config";
my $username = $config->{username} || die "No username in config";
my $password = $config->{password} || die "No password in config";
my $use_ssl = $config->{ssl};
my $rules = $config->{rules} || die "No rules in config";

if ($showme >= 0)
{
  print Dumper({
    criteria => { @{$rules->[$showme]->{criteria}} },
    action => $rules->[$showme]->{action},
  });
  exit;
}

# Create the connection
my $imap = $type->new($server, use_ssl => $use_ssl) ||
           die "Unable to connect to IMAP: $Net::IMAP::Simple::errstr\n";

# Log on
if(!$imap->login($username, $password)){
  print STDERR "Login failed: " . $imap->errstr . "\n";
  exit(64);
}

my $message_count = $imap->select('INBOX');

printf "Message count is %s\n", $message_count;
#exit;

my @indexes = (1 .. $message_count);
@indexes = reverse(@indexes) if $reverse;

my $actions_taken = 0;

MESSAGE:
foreach my $i (@indexes)
{

  next MESSAGE unless $imap->top($i);
  my $es = Email::Simple->new(join '', @{ $imap->top($i) } );
  my $flags = Set::Scalar->new(map { s/^\\//g; $_ } $imap->msg_flags($i));

  my $action = 'NOTHING';
  my $matched_rule = -1;

  RULE:
  foreach my $r ( 0 .. $#$rules )
  {
    my $rule = $rules->[$r];
    my @criteria = @{$rule->{criteria} || []};

    my $result = 1;

    while (@criteria)
    {
      my $type = shift @criteria;
      my $value = shift @criteria;

      if ($type eq 'has_flag') {
        $result &&= $flags->has($value);
      }
      elsif ($type eq 'from_matches') {
        $result &&= $es->from =~ /$value/;
      }
      elsif ($type eq 'to_matches') {
        $result &&= $es->to =~ /$value/;
      }
      elsif ($type eq 'replyto_matches') {
        $result &&= $es->replyto =~ /$value/;
      }
      elsif ($type eq 'subject_matches') {
        $result &&= $es->subject =~ /$value/;
      }
      elsif ($type eq 'older_than') {
        $result &&= $es->age > $value;
      }
      else {
          die "Invalid criteria type $type at rule $r";
      }
    }

    if ($result) {
      $action = $rule->{action};
      $matched_rule = $r;
      last RULE;
    }

  }

  next MESSAGE if $matched_rule < 0;

  if (uc($action) eq 'KEEP') {
    printf "KEEP (R%s) %s (%sd) from %s: %s\n", $matched_rule, $i, $es->age, $es->from, $es->subject if $debug;
  }
  elsif (uc($action) eq 'ARCHIVE') {
    printf "ARCHIVE (R%s) %s (%sd) from %s: %s\n", $matched_rule, $i, $es->age, $es->from, $es->subject if $verbose;
    $imap->archive($i) || die "Failed to ARCHIVE";
  }
  elsif (uc($action) eq 'DELETE') {
    printf "DELETE (R%s) %s (%sd) from %s: %s\n", $matched_rule, $i, $es->age, $es->from, $es->subject if $verbose;
    $imap->delete($i) || die "Failed to DELETE";
  }
  else {
    die "Unknown action $action from rule $matched_rule";
  }

  $actions_taken++;

  last if $actions_taken >= $limit;

  unless ($actions_taken % $interval)
  {
    print "Expunging\n";
    $imap->expunge_mailbox('INBOX');
  }

}

print "Expunging\n";
$imap->expunge_mailbox('INBOX');
$imap->quit;

exit;

package Email::Simple;

use Encode;
use POSIX qw(floor);
use Set::Scalar;

BEGIN { $Date::Manip::Backend = 'DM5'; }
use Date::Manip;

use strict;

sub subject
{
  my $self = shift;
  Encode::decode('MIME-Header', $self->header('Subject'))
}

sub to
{
  my $self = shift;
  Encode::decode('MIME-Header', $self->header('To'))
}

sub replyto
{
  my $self = shift;
  Encode::decode('MIME-Header', $self->header('Reply-to'))
}

sub from
{
  my $self = shift;
  Encode::decode('MIME-Header', $self->header('From'))
}

sub date
{
  my $self = shift;
  $self->header('Date');
}

sub age
{
  my $self = shift;
  days_ago($self->date);
}

## not methods

sub days_ago
{
    my $datestr = shift;
    epoch_days(ParseDate('today')) - epoch_days(ParseDate($datestr));
}

sub epoch_days
{
    my $date = shift; # already parsed
    floor(UnixDate($date, "%s") / (60*60*24));
}

package Net::IMAP::Simple;

sub archive {
  my $self = shift;
  my $i = shift;
  $self->copy($i, 'Archive') && $self->delete($i);
}

package Net::IMAP::Simple::Gmail;

sub archive {
  my $self = shift;
  my $i = shift;
  $self->delete($i);
}


__END__

