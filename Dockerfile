
FROM registry.gitlab.com/rfugina/docker-ubuntu-perl
MAINTAINER Rob Fugina <rfugina@wustl.edu>

COPY imap_maintenance.pl /root/

# make sure we've got all the modules we need
RUN perl -cw /root/imap_maintenance.pl

ENTRYPOINT ["/usr/bin/perl", "/root/imap_maintenance.pl"]

