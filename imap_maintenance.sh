#!/bin/bash

set -e

cd $(dirname $0)

docker build -t imap-maintenance .

docker run -it --rm -v $(pwd):$(pwd) -w $(pwd) imap-maintenance "$@"

